module.exports = {
  future: {},
  content: [
    // dev gatsby-theme
    // "./../gatsby-*/src/**/*.{js,jsx,ts,tsx}",
    // dev site
    "./node_modules/@onegeo/**/*.{js,jsx,ts,tsx}",
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  daisyui: {
    themes: [
      {
        onegeo: {
          primary: "#2391C0",
          secondary: "#345230",
          accent: "#37CDBE",
          neutral: "#3D4451",
          "base-100": "#FAFAFA",
          info: "#3ABFF8",
          success: "#36D399",
          warning: "#FBBD23",
          error: "#F87272",
        },
      },
      "light",
      "dark",
    ],
  },
  theme: {
    extend: {
      height: {
        xs: "6rem", // h-24 96px
        sm: "10rem", // h-40 160px
        base: "14rem", // h-56 224px
        lg: "18rem", // h-72 288px
        xl: "24rem", // h-96 384px
        "2xl": "32rem", // - 512px
      },
      width: {
        xs: "6rem", // w-24 96px
        sm: "10rem", // w-40 160px
        base: "14rem", // w-56 224px
        lg: "18rem", // w-72 288px
        xl: "24rem", // w-96 384px
        "2xl": "32rem", // - 512px
      },
    },
  },
  safelist: [
    {
      pattern: /m.+/,
    },
    {
      pattern: /p.+/,
    },
    {
      pattern: /bg.+/,
      variants: ["hover"],
    },
    {
      pattern: /text.+/,
    },
    {
      pattern: /shadow.+/,
    },
    {
      pattern: /rounded.+/,
    },
  ],
  plugins: [
    require("daisyui"),
    require("@tailwindcss/typography"),
    require("@tailwindcss/forms"),
  ],
}
