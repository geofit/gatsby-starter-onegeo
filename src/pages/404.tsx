import React, { ReactElement } from "react"
import { Link } from "gatsby"

interface Props {}

function Error404(_props: Props): ReactElement {
  //const [isMenuOpen, setMenuOpen] = useState(false)
  return (
    <div className="bg-base-100 min-h-screen px-4 py-16 sm:px-6 sm:py-24 md:grid md:place-items-center lg:px-8">
      <div className="max-w-max mx-auto">
        <main className="sm:flex">
          <p className="text-4xl font-extrabold text-primary sm:text-5xl">
            404
          </p>
          <div className="sm:ml-6">
            <div className="sm:border-l sm:border-neutral sm:pl-6">
              <h1 className="text-4xl font-extrabold text-neutral tracking-tight sm:text-5xl">
                Page introuvable
              </h1>
              <p className="mt-1 text-base text-neutral">
                Merci de vérifier l'URL dans la barre de recherche et réessayer.
              </p>
            </div>
            <div className="mt-10 flex space-x-3 sm:border-l sm:border-transparent sm:pl-6">
              <Link
                to="/"
                className="inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-primary-content bg-primary hover:bg-primary-focus focus:outline-none focus:ring-2 focus:ring-offset-2"
              >
                Retour sur la page d'acceuil
              </Link>
              <Link
                to="/contact"
                className="inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md text-secondary-content bg-secondary hover:bg-secondary-focus focus:outline-none focus:ring-2 focus:ring-offset-2"
              >
                Contactez-nous
              </Link>
            </div>
          </div>
        </main>
      </div>
    </div>
  )
}

export default Error404
