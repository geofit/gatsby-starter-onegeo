import React, { ReactElement } from "react"
import { graphql, PageProps } from "gatsby"

// import { Layout, Button } from "@onegeo/gatsby-theme-onegeo"
// import { UserMenu } from "@onegeo/gatsby-plugin-auth"

function Index({ data }: PageProps): ReactElement {
  // @ts-ignore
  const site = data.site.siteMetadata

  return (
    <>
      {/* <Layout> */}
      <section>
        <div className="w-full bg-cover bg-center bg-[url('../images/fond.png')] h-160 z-0 pt-20">
          <div className="relative px-4 py-16 sm:px-6 sm:py-24 lg:py-20 lg:px-8">
            <h1 className="text-center text-4xl font-extrabold tracking-tight sm:text-5xl lg:text-6xl">
              <span className="block text-base-100">{site.title}</span>
              <span className="block text-primary">{site.subtitle}</span>
            </h1>
            <p className="mt-6 max-w-lg mx-auto text-center text-xl text-base-200 sm:max-w-3xl">
              {site.description}
            </p>
            <div className="mt-10 max-w-sm mx-auto sm:max-w-none sm:flex sm:justify-center">
              <div className="space-y-4 sm:space-y-0 sm:mx-auto sm:inline-grid sm:grid-cols-1 sm:gap-5">
                {/* <Button to="/404" className="btn-primary">
                  Découvrez nos cartes interactives
                </Button> */}
              </div>
            </div>
          </div>
        </div>
      </section>
      <div className="w-full text-center text-2xl mt-10">
        Gatsby Starter pour Onegeo Portal
      </div>
      {/* </Layout> */}
    </>
  )
}

export default Index

export const query = graphql`
  query {
    site {
      siteMetadata {
        title
        subtitle
        description
      }
    }
  }
`
