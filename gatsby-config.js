var dotenv = require("dotenv")
var dotenvExpand = require("dotenv-expand")
var myEnv = dotenv.config()
dotenvExpand.expand(myEnv)

module.exports = {
  flags: {
    //PRESERVE_WEBPACK_CACHE: true,
    //PRESERVE_FILE_DOWNLOAD_CACHE: true,
    //FAST_DEV: true,
    //FAST_REFRESH: true,
    DEV_SSR: false,
  },

  siteMetadata: {
    title: `Onegeo Portal`,
    subtitle: `Hub géospatial collaboratif`,
    description: `Solution Open Source sur-mesure pour communiquer et valoriser les données disponibles sur votre territoire ou vos infrastructures`,
    author: `GEOFIT`,
    siteUrl: process.env.PROJECT_PROTOCOL + "://" + process.env.PROJECT_HOST,
    theme: "onegeo",
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-typescript`,
    `gatsby-plugin-image`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-sitemap`,
    `gatsby-plugin-postcss`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: "gatsby-plugin-eslint",
      options: {
        extensions: ["js", "jsx", "ts", "tsx"],
        exclude: ["node_modules", "bower_components", ".cache", "public"],
        stages: ["develop", "build-javascript"],
        emitWarning: true,
        failOnError: false,
      },
    },
    // {
    //   resolve: "@onegeo/gatsby-plugin-auth",
    //   options: {},
    // },
    {
      resolve: "@onegeo/gatsby-theme-onegeo",
      options: {},
    },

    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
