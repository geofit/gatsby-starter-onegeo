<h1 align="center">
Gatsby Onegeo Portal Starter
</h1>

This repo hosted on git.geofit.fr is mirrored to gitlab.com (read only) for gatsby CLI

## 🚀 Quick start

1.  **Create a Gatsby site.**

    Use the Gatsby CLI to create a new site, specifying the starter.

    ```bash
    gatsby new portal https://gitlab.com/geofit/gatsby-starter-onegeo.git
    ```

    **Note**: @onegeo/gatsby-theme-onegeo is already installed with this template.

    From portal folder

    Edit portal .env file (from .sample.env)

    ```bash
    cd portal
    mv .sample.env .env
    nano .env
    ```

    ```bash
    PROJECT_HOST=
    PROJECT_PORT=443
    PROJECT_PROTOCOL=https
    PROJECT_URL=${PROJECT_PROTOCOL}://${PROJECT_HOST}:${PROJECT_PORT}

    DIRECTUS_URL=${PROJECT_URL}/directus/
    DIRECTUS_TOKEN=

    TYPESENSE_API_TOKEN=
    ```

1.  **Start developing.**

    Navigate into your new site’s directory and start it up.

    ```bash
    gatsby develop
    ```

1.  **Open the source code and start editing!**

    Your site is now running at http://localhost:8000

    **Note**: You'll also see a second link: http://localhost:8000/\_\_\_graphql. This is a tool you can use to experiment with querying your data. Learn more about using this tool in the Gatsby tutorial.

    Open the my-default-starter directory in your code editor of choice and edit src/pages/index.js. Save your changes and the browser will update in real time!

1.  **Initial Commit & Push to Git repository**

    You need to create a empty project first on gitlab.

    From command line

    ```bash
    cd portal/
    git remote add origin https://git.geofit.fr/{group}/{project_name}.git
    git add .
    git commit -m "Initial commit"
    git push -u origin main
    ```

    From VS Code  
    Todo...

## Config gatsby-theme-onegeo

TODO...

## 🎓 Learning Gatsby

Looking for more guidance? Full documentation for Gatsby lives [on the website](https://www.gatsbyjs.org/). Here are some places to start:

- **For most developers, we recommend starting with our [in-depth tutorial for creating a site with Gatsby](https://www.gatsbyjs.org/tutorial/).** It starts with zero assumptions about your level of ability and walks through every step of the process.

- **To dive straight into code samples, head [to our documentation](https://www.gatsbyjs.org/docs/).** In particular, check out the _Guides_, _API Reference_, and _Advanced Tutorials_ sections in the sidebar.

## License

MIT
